package com.xjj.mall.common.constant;


/**
 * 常量
 * @author zhanghejie
 */
public interface CommonConstant {

    /**
     * 限流标识
     */
    String LIMIT_ALL="XMALL_LIMIT_ALL";

}
